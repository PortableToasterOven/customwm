#include "declaration.hpp"
namespace sandbox {

    class heap {

        public:
            heap(int inputArray[], int _size);
            ~heap();
            void insert(int input);
            int remove();
            void printAll();

        private:
            int* data;
            uint32_t size;
            uint32_t insertIdx;

            uint32_t descendParentGreater(int parent);
            uint32_t ascendChild(int child);
            void swapData(int& dataA, int& dataB);
    };
}
