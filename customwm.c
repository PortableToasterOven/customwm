/* This software is in the public domain
 * and is provided AS IS, with NO WARRANTY. */

#include <X11/Xlib.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX(a, b) ((a) > (b) ? (a) : (b))

static int trappedErrorCode = 0;
static int (*oldErrorHandler) (Display*, XErrorEvent*);

static int errorHandler(Display* display, XErrorEvent* error)
{
    trappedErrorCode = error->error_code;
    createDebugWindow(display, error, "Trapped error?", 0xff4444);
    return 0;
}

void trappedError(Display* display, XErrorEvent* error)
{
    trappedErrorCode = 0;
    oldErrorHandler = XSetErrorHandler(errorHandler);
}

int untrappedError(Display* display, XErrorEvent* error)
{
    XSetErrorHandler(oldErrorHandler);
    return trappedErrorCode;
}

//oh boy, doing things the weird and wacky way 
void createTwinWindow(Display* display, XEvent* evt, char* msg, unsigned long foreground) 
{
    Window root = DefaultRootWindow(display);

    int x = 2;
    int y = 2;

    Window winC = XCreateSimpleWindow(display, // parent
                                           root,
                                           x+2010, y+1100,        // x, y
                                           200, 30,    // w, h
                                           1, 0xAAAAAA, // border_width, border_color
                                           0x330000);   // background

    Window winP = XCreateSimpleWindow(display,
                                           root,
                                           x, y+100,        // x, y
                                           300, 200,    // w, h
                                           1, 0xAAAAAA, // border_width, border_color
                                           0x000022);   // background

    XReparentWindow(display,
                    winC,
                    winP,
                    0, 0);  // Offset of client window within windowFrame.
    XSelectInput(display, winP, SubstructureNotifyMask | SubstructureRedirectMask | ExposureMask );
    XSelectInput(display, winC, SubstructureNotifyMask | SubstructureRedirectMask | ExposureMask );
    XMapWindow(display, winP);
    XMapWindow(display, winC);

    int screen = DefaultScreen(display);
    GC gc = DefaultGC(display, screen); 
    XSetForeground(display, gc, foreground);
    XDrawString(display, 
                winC, 
                gc,
                6, 18, // x, y
                msg, strlen(msg));
}


void createDebugWindow(Display* display, XEvent* evt, char* msg, unsigned long foreground, int x, int y) 
{
    Window root = DefaultRootWindow(display);

    Window dbgWindow = XCreateSimpleWindow(display,
                                           root,
                                           x, y,        // x, y
                                           200, 30,    // w, h
                                           1, 0xAAAAAA, // border_width, border_color
                                           0x222222);   // background

    XSelectInput(display, dbgWindow, ExposureMask | KeyPressMask);
    XMapWindow(display, dbgWindow);

    int screen = DefaultScreen(display);

/*
    char label[] = "parent height: ";

    XWindowAttributes dbgAttr;
    XGetWindowAttributes(display, evt->xmaprequest.parent, &dbgAttr);
    int dbgMsg = dbgAttr.height;
    char temp[100]; // ummm wow this took 5 lines
    snprintf(temp, sizeof dbgMsg, "%zu", dbgMsg);

    strcat(label, temp); 
        

*/
    GC gc = DefaultGC(display, screen); 
    XSetForeground(display, gc, foreground);
    XDrawString(display, 
                dbgWindow, 
                gc,
                6, 18, // x, y
                msg, strlen(msg));
}

void createDebugWindowXY(Display* display, XEvent* evt, char* msg, unsigned long foreground) 
{
    int x = rand() % 1200;
    int y = rand() % 700;
    createDebugWindow(display, evt, msg, foreground, x, y); 
}

createTraceWindow(Display* display, Window window, int x, int y)
{
    char label[] = "This is window id: ";
    char temp[100];
    snprintf(temp, sizeof window, "%d", window);

    XEvent* dummy;
    strcat(label, temp); 
    createDebugWindow(display, dummy, label, 0xffffff, x, y);
}

int main(void) 
{
    Display * display;

    /*If we can't open display return 1*/    
    if(!(display = XOpenDisplay(0x0))) {
        return 1;
    }
    XSync(display, False);
    XSetErrorHandler(trappedError);

    const Window root = DefaultRootWindow(display);

    XSelectInput(display,
                 root,
                 PointerMotionMask |
                 ButtonMotionMask |
                 KeyPressMask |
                 ButtonPressMask |
                 ButtonReleaseMask |
                 SubstructureRedirectMask |
                 SubstructureNotifyMask);

    //XMapWindow(display, windowFrame);
    //Cursor d = XCreateFontCursor(display, 2);


//TODO: read up on this
    XGrabKey(display, XKeysymToKeycode(display, XStringToKeysym("q")), Mod1Mask,
        root, True, GrabModeAsync, GrabModeAsync);
    XGrabKey(display, XKeysymToKeycode(display, XStringToKeysym("d")), Mod1Mask,
        root, True, GrabModeAsync, GrabModeAsync);
    XGrabKey(display, XKeysymToKeycode(display, XStringToKeysym("r")), Mod1Mask,
        root, True, GrabModeAsync, GrabModeAsync);
    XGrabKey(display, XKeysymToKeycode(display, XStringToKeysym("0")), Mod1Mask,
        root, True, GrabModeAsync, GrabModeAsync);
    XGrabKey(display, XKeysymToKeycode(display, XStringToKeysym("Return")), Mod1Mask,
        root, True, GrabModeAsync, GrabModeAsync);
    XGrabKey(display, XKeysymToKeycode(display, XStringToKeysym("f")), Mod1Mask,
        root, True, GrabModeAsync, GrabModeAsync);

    XGrabButton(display, 1, Mod1Mask, root, True,
        ButtonPressMask|ButtonReleaseMask|PointerMotionMask, GrabModeAsync, GrabModeAsync, None, None);
    XGrabButton(display, 3, Mod1Mask, root, True,
        ButtonPressMask|ButtonReleaseMask|PointerMotionMask, GrabModeAsync, GrabModeAsync, None, None);
//end readup

    XWindowAttributes attributes;
    XWindowAttributes ib;
    XButtonEvent start;
    XEvent event; 
    Window foc;

    start.subwindow = None;  

    int revert_to;



    while(True) {
        XNextEvent(display, &event);
        XGetInputFocus(display, &foc, &revert_to);

        switch(event.type) {
            
            case KeyPress:
                createTraceWindow(display, root, 1000, 20);
                if(event.xbutton.subwindow != None) {   
                    if(event.xkey.keycode == XKeysymToKeycode(display, XStringToKeysym("q"))) { 
                        XDestroyWindow(display, foc);
                    }
                    else if (event.xkey.keycode == XKeysymToKeycode(display, XStringToKeysym("d"))) { 
                        XLowerWindow(display, foc);
                    }
                    else if (event.xkey.keycode == XKeysymToKeycode(display, XStringToKeysym("r"))) { 
                        XRaiseWindow(display, foc);
                    }
                }
                else {
                    if(event.xkey.keycode == XKeysymToKeycode(display, XStringToKeysym("d"))) { 
                        system("rofi -show run");
                    }
                    else if(event.xkey.keycode == XKeysymToKeycode(display, XStringToKeysym("0"))) { 
                        XCloseDisplay(display);
                    }
                }
                break;

            case ButtonPress:
                if(event.xbutton.subwindow != None) {
                    XGetWindowAttributes(display, event.xbutton.subwindow, &ib);

                    if(!ib.override_redirect) {
                        /*Save the windows size and location before doing anything*/ 
                        XGetWindowAttributes(display, event.xbutton.subwindow, &attributes);
                        if(foc != event.xbutton.subwindow)
                        XSetInputFocus(display, event.xbutton.subwindow, RevertToParent, CurrentTime); 
                        XRaiseWindow(display, start.subwindow);
                    }
                  
                }
                break;

            case ButtonRelease:
                start.subwindow = None;
                break;

            case MotionNotify:
                if(start.subwindow != None) {
                    int xdiff = event.xbutton.x_root - start.x_root;
                    int ydiff = event.xbutton.y_root - start.y_root;
                    XMoveResizeWindow(display, start.subwindow,
                        attributes.x + (start.button==1 ? xdiff : 0),
                        attributes.y + (start.button==1 ? ydiff : 0),
                        /*Minimum window size when resizing is 100x50 pixels because
                        *I don't think there is any need for smaller windows*/
                        MAX(100, attributes.width + (start.button==3 ? xdiff : 0)),
                        MAX(50, attributes.height + (start.button==3 ? ydiff : 0)));
                }
                break;

            case UnmapNotify:
                
                //Const Window windowFrame = 

                break;

            case MapRequest:
            {
                const unsigned int  BORDER_WIDTH = 4;
                const unsigned long BORDER_COLOR = 0xff8000;
                const unsigned long BG_COLOR     = 0x0000000F;

                Window windowMap = event.xmaprequest.window;

                XGetWindowAttributes(display, windowMap, &attributes);               

                // 2. TODO - see Framing Existing Top-Level Windows section below.

                // 3. Create windowFrame.
                const Window windowFrame = XCreateSimpleWindow(display,
                                                         root,
                                                         attributes.x,
                                                         attributes.y,
                                                         attributes.width,
                                                         attributes.height,
                                                         BORDER_WIDTH,
                                                         BORDER_COLOR,
                                                         BG_COLOR);
                // 3. Select events on windowFrame.
                XSelectInput(display,
                             windowFrame,
                             SubstructureRedirectMask | SubstructureNotifyMask);

                // 4. Add client to save set, so that it will be restored and kept alive if we crash.
                XAddToSaveSet(display, windowMap);

                // 5. Reparent client window.
                XReparentWindow(display,
                                windowMap,
                                windowFrame,
                                0, 0);  // Offset of client window within windowFrame.

                // 6. Map windowFrame.
                XMapWindow(display, windowFrame);
                XMapWindow(display, windowMap);
                // 7. Save windowFrame handle.
                //clients[windowMap] = windowFrame;

                // 8. Grab events for window management actions on client window.
                //   a. Move windows with alt + left button.
                //XGrabButton(...);
                //   b. Resize windows with alt + right button.
                //XGrabButton(...);
                //   c. Kill windows with alt + f4.
                //XGrabKey(...);
                //   d. Switch windows with alt + tab.
                //XGrabKey(...);
                createTraceWindow(display, windowFrame, 600, 60);
                createTraceWindow(display, windowMap, 600, 20);
                break;
            }
            case ConfigureNotify:
            {
                XConfigureRequestEvent eCfg = event.xconfigurerequest;

                XWindowChanges changes;
                changes.x = eCfg.x;
                changes.y = eCfg.y;
                changes.width = eCfg.width;
                changes.height = eCfg.height;
                changes.border_width = eCfg.border_width;
                changes.sibling = eCfg.above;
                changes.stack_mode = eCfg.detail;

                XConfigureWindow(display, eCfg.window, eCfg.value_mask, &changes);
                break;
            }
            case CreateNotify:
                break;
            case DestroyNotify: 
                break;
            case ReparentNotify:
                break;
        }
   }
}

