#include "heap.hpp"
namespace sandbox {

    heap::heap(int inputArray[], int _size) {
        size = _size;

        insertIdx = 0;
        data = new int[size];

        for(int i = 0; i < size; ++i)
            insert(inputArray[i]);
    }

    heap::~heap() {
        delete[] data;
    }

    void heap::insert(int input) {

        data[insertIdx] = input;
        insertIdx += 1;

        uint32_t child = insertIdx - 1;
        uint32_t parent = ascendChild(child);
        if(parent == std::numeric_limits<uint32_t>::max()) {
            return;
        }

        while(data[child] > data[parent]) {

            swapData(data[child], data[parent]);
            
            child = (child  - 1) >> 1;
            parent = ascendChild(child);
            if(parent == std::numeric_limits<uint32_t>::max()) {
                return;
            }
        }
    }

    int heap::remove() {

        if(insertIdx == 0)
            throw std::runtime_error("ERROR.heap: remove() called, but size is already zero!\n");
        else {

            int removed = data[0];
            insertIdx -= 1;
            data[0] = data[insertIdx];
            
            size -= 1;

            uint32_t parent = 0;
            uint32_t child = descendParentGreater(parent);
            if(child == std::numeric_limits<uint32_t>::max()) {
                return removed;
            }

            while(data[child] > data[parent]) {

                swapData(data[child], data[parent]);

                parent = child;
                child = descendParentGreater(parent);
                if(child == std::numeric_limits<uint32_t>::max()) {
                    return removed;
                }
            }

            return removed;
        }
    }

    void heap::printAll() {

        uint32_t level = 1;

        for(uint32_t i = 0; i<size; ++i) {

            if(i == level-1) {
                std::cout<<"\n";
                level = level << 1;
            }

            std::cout<<data[i]<<" ";
        }

        std::cout<<"\n";
    }


    uint32_t heap::descendParentGreater(int parent) {

        uint32_t child = parent + 1 << 1;

        if(child < size) { 
            if(data[(child - 1)] > data[(child)]) {
                child -= 1;
            }
        }
        else if(child - 1 < size) {
            child -= 1;
        }
        else { 
            return std::numeric_limits<uint32_t>::max();
        }  

        return child;
    }

    uint32_t heap::ascendChild (int child) {

        if(child == 0) {
            return std::numeric_limits<uint32_t>::max();
        }

        return (child  - 1) >> 1;
    }

    void heap::swapData(int& dataA, int& dataB) {
        int temp = dataA;
        dataA = dataB;
        dataB = temp;
    }
}
